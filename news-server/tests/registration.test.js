const { register } = require('../controllers/users');

let users = [];

const mockResponse = () => {
  const res = {};
  res.send = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};

const mockRequest = body => ({
  body,
});

const mockNewUser = {
  email: "test@mail.com",
  password: "12345678",
  login: "Test User",
  avatar: ""
};

jest.mock('../models', () => ({
  User: {
    create: jest.fn((body) => {
      users.push(body);
      delete body.password
      return Promise.resolve({
        ...body,
        toJSON() {
          return JSON.stringify(body)
        }
      });
    })
  }
}));

describe('registration', () => {
  test('should create new user if success', async () => {
    const res = mockResponse();
    const req = mockRequest(mockNewUser);
    await register(req, res);
    expect(users[0].email).toEqual("test@mail.com");
    expect(res.json).toHaveBeenCalled();
  });

  test('should return error if body is invalid', async () => {
    users.splice(0, users.length);
    const res = mockResponse();
    const req = mockRequest({});
    await register(req, res);
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.send).toHaveBeenCalledWith("User was not created");
    expect(users.length).toBe(0);
  });
});
