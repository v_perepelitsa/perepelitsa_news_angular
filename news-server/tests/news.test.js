const request = require('supertest');
const app = require('../app');
const { createNews, getAllNews } = require('../controllers/news');

const mockedNews = [
  { user_id: 1, title: 'Test1', tags: 'Tag1', content: 'Content1', images: ""},
  { user_id: 2, title: 'Test2', tags: 'Tag2', content: 'Content2', images: ""},
  { user_id: 3, title: 'Test3', tags: 'Tag3', content: 'Content3', images: ""}
];
const requestBody = {news: {user_id: 3, title: 'Test', tags: 'Test', content: 'Test', images: ''}};

const mockResponse = () => {
  const res = {};
  res.send = jest.fn().mockReturnValue(res);
  res.sendStatus = jest.fn().mockReturnValue(res);
  res.json = jest.fn().mockReturnValue(res);
  return res;
};

const mockRequest = body => ({
  body,
});

jest.mock('../models', () => ({
  News: {
    create: jest.fn((body) => {
      mockedNews.push(body);
      return Promise.resolve(body);
    }),
    findAll: jest.fn(() => {
      return Promise.resolve(mockedNews);
    })
  }
}));

describe('news', () => {
  test('should add news if body is correct', async () => {
    const news = {
      user_id: 3,
      title: 'Test',
      tags: 'Test',
      content: 'Test',
      images: ''
    }
    const res = mockResponse();
    const req = mockRequest(requestBody);
    await createNews(req, res, () => {});
    expect(res.send).toHaveBeenCalledWith(news);
    expect(mockedNews[3]).toEqual(news);
  });

  test('should return 500 if body is not correct', async () => {
    const res = mockResponse();
    const req = mockRequest({});
    await createNews(req, res, () => {});
    expect(res.sendStatus).toHaveBeenCalledWith(500);
  });

  test('should return all news', async () => {
    const res = mockResponse();
    const req = mockRequest({});
    await getAllNews(req, res)
    expect(res.send).toHaveBeenCalledWith(mockedNews);
  });

  test('GET /news with invalid path should return 404', async () => {
    const response = await request(app).get('/new');
    expect(response.statusCode).toEqual(404);
  });
});
