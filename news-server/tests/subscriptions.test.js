const { unsubscribe } = require('../controllers/subscriptions');

const mockedSubscriptions = [
  { follower_id: "1", subscription_id: "5"},
  { follower_id: "2", subscription_id: "4"},
  { follower_id: "1", subscription_id: "8"},
];

const mockResponse = () => {
  const res = {};
  res.send = jest.fn().mockReturnValue(res);
  res.status = jest.fn().mockReturnValue(res);
  return res;
};

const mockRequest = query => ({
  query,
});


jest.mock('../models', () => ({
  Subscription: {
    create: jest.fn((body) => {
      mockedNews.push(body);
      return Promise.resolve({
        toJSON() {
          return JSON.stringify(body)
        }
      });
    }),
    destroy: jest.fn(({where: { follower_id, subscription_id }}) => {
      const currentSubIndex = mockedSubscriptions.findIndex((item) => item.follower_id === follower_id && item.subscription_id === subscription_id);
      if (currentSubIndex != -1) {
        mockedSubscriptions.splice(currentSubIndex, 1);
        return Promise.resolve(1);
      }
      return Promise.resolve(0);
    })
  }
}));

describe('subscription', () => {
  test('should delete if query params match', async () => {
    const res = mockResponse();
    const req = mockRequest({follower: "1", subscription: "5"});
    await unsubscribe(req, res);
    expect(res.status).toHaveBeenCalledWith(200);
    expect(mockedSubscriptions.length).toBe(2);
  });

  test('should return 404 if some params not match by deleting', async () => {
    const res = mockResponse();
    const req = mockRequest({follower: "10", subscription: "50"});
    await unsubscribe(req, res);
    expect(res.send).toHaveBeenCalledWith({"message": "Not found", "status": 404});
  });
});

