const express = require("express");
const router = express.Router();
const upload = require("../utils//uploads");
const passport = require("passport");
const { register, login, checkUser, googleAuth, getUserById, updateUser } = require("../controllers/users");
const auth = passport.authenticate("jwt", { session: false });

router.post("/register", register);
router.post("/login", login);
router.get("/me", auth, checkUser);
router.post("/googleAuth", googleAuth);
router.get("/:id", auth, getUserById);
router.patch("/:id", upload.single("image"), updateUser);

module.exports = router;