const express = require("express");
const { subscribe, unsubscribe } = require("../controllers/subscriptions");
const router = express.Router();

router.post("/", subscribe);
router.delete("/", unsubscribe);

module.exports = router;