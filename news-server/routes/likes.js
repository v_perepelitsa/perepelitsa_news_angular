const express = require("express");
const { addLike, getLikes, deleteLike } = require("../controllers/likes");
const router = express.Router();

router.post("/", addLike);
router.get("/", getLikes);
router.delete("/", deleteLike);

module.exports = router;
