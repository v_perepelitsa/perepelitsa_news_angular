const express = require('express');
const { getAllNews, createNews } = require('../controllers/news');
const upload = require('../utils//uploads')
const router = express.Router();

router.get('/', getAllNews);
router.post('/', upload.single('image'), createNews)

module.exports = router;
