const db = require('../models/index');

module.exports = {
  async getAllNews(req, res, next) {
    try {
      const news = await db.News.findAll({
        order: [['createdAt', 'DESC']],
        include:  [
          {model: db.User, as: "user"},
          {model: db.Like, as: "likes", attributes: ['user_id']}
        ]
      });
      res.send(news);
    } catch (error) {
      res.send(error)
    }
  },
  
  async createNews(req, res, next) {
    try {
      const { user_id, tags, title, content} = req.body.news;
      let news = await db.News.create({
        user_id,
        title,
        tags,
        content,
        images: req.file ? req.file.path : ""
      });
      res.send(news);
    } catch (error) {
      res.sendStatus(error.status || 500);
    }
  }
};
