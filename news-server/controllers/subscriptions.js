const db = require("../models/index");

module.exports = {
  async subscribe(req, res) {
    try {
      const { follower_id, subscription_id } = req.body;
      console.log(req.body);
      let subscription = await db.Subscription.create({
        follower_id,
        subscription_id,
      });
      res.send(subscription);
    } catch (error) {
      res.status(500);
    }
  },
  
  async unsubscribe(req, res) {
    try {
      const follower_id = req.query.follower;
      const subscription_id = req.query.subscription;
      const result = await db.Subscription.destroy({
        where: {
          follower_id,
          subscription_id
        },
      });
      if (result === 0) {
        throw {
          status: 404,
          message: "Not found",
        };
      } else {
        res.status(200);
      }
    } catch (error) {
      res.send(error)
    }
  }
};