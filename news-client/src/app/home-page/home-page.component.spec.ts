import { TestBed } from "@angular/core/testing";
import { NgxPaginationModule } from "ngx-pagination";
import { SearchPipe } from "../pipes/search.pipe";
import { NewsService } from "../services/news.service";
import { HomePageComponent } from "./home-page.component";

const newsServiceStub: Partial<NewsService> = {
  getNews() {},
};

describe('HomePageComponent', () => {
  let component: HomePageComponent;
  let service: NewsService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HomePageComponent, SearchPipe],
      imports: [NgxPaginationModule],
      providers: [
        { provide: NewsService, useValue: newsServiceStub },
      ]
    }).compileComponents();
    const fixture = TestBed.createComponent(HomePageComponent);
    component = fixture.debugElement.componentInstance;
    service = TestBed.inject(NewsService);
  });

  it('should fetch news by init component', () => {
    spyOn(service, 'getNews');
    component.ngOnInit();
    expect(service.getNews).toHaveBeenCalled();
	});

  it('should set search settings when filter is changing', () => {
    component.setSearch({ searchFilter: 'tags', searchString: 'news'});
    expect(component.searchFilter).toBe('tags');
    expect(component.searchString).toBe('news');
  });
});
