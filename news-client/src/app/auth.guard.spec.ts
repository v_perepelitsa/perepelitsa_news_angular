import { TestBed } from "@angular/core/testing";
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from "@angular/router";
import { AuthGuard } from "./auth.guard";
import { AuthService } from "./services/auth.service";
import { SocialAuthService } from "angularx-social-login";

function fakeRouterState(url: string): RouterStateSnapshot {
  return {
    url,
  } as RouterStateSnapshot;
};

const mockUser = {
  user: {
    id: 1,
    email: 'test',
    login: 'test',
    subscriptions: []
  }
};

const authServiceStub = {};

describe('AuthGuard', () => {
  const dummyRoute = {} as ActivatedRouteSnapshot;
  let guard: AuthGuard;
  let routerSpy: jasmine.SpyObj<Router>;
  let service: AuthService;

  beforeEach(() => {
    localStorage.clear();
    routerSpy = jasmine.createSpyObj<Router>('Router', ['navigate']);
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [
        AuthGuard,
        { provide: Router, useValue: routerSpy },
        AuthService,
        { provide: SocialAuthService, useValue: {}}
      ]
    });

    guard = TestBed.get(AuthGuard);
    service = TestBed.inject(AuthService);
  });

  it('should return false if no current user', () => {
    const canActivate = guard.canActivate(dummyRoute, fakeRouterState('/user/2'));
    expect(canActivate).toBeFalse();
  });

  it('should return true if current user exists', () => {
    service.currentUserSubject.next(mockUser);
    const canActivate = guard.canActivate(dummyRoute, fakeRouterState('/user/2'));
    expect(canActivate).toBeTrue();
  })
});
