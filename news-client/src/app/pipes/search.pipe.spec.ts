import { INews } from "../services/news.service";
import { SearchPipe } from "./search.pipe";

const mockNews = [
  {
    tags: 'news text',
    title: 'Great News',
    content: 'Content',
    user: {
      login: 'Bulgakov'
    }
  },
  {
    tags: 'test tag',
    title: 'Another',
    content: 'News',
    user: {
      login: 'Pushkin'
    }
  },
  
]

describe('SearchPipe', () => {
	let pipe: SearchPipe;

	beforeEach(() => {
		pipe = new SearchPipe();
	});

	it('should return all news if search is empty string', () => {
    const result = pipe.transform(mockNews as INews[], 'all', '')
		expect(result.length).toEqual(2);
	});

	it('should return matched news by filter and search', () => {
    let result = pipe.transform(mockNews as INews[], 'user', 'Bulgakov');
		expect(result).toEqual([ mockNews[0] as INews ]);
    result = pipe.transform(mockNews as INews[], 'tags', 'tag');
    expect(result).toEqual([ mockNews[1] as INews ]);
    result = pipe.transform(mockNews as INews[], 'all', 'News');
    expect(result).toEqual(mockNews as INews[]);
	});

});