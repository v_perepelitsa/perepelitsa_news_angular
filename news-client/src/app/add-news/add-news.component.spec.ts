import { ComponentFixture, TestBed } from "@angular/core/testing";
import { FormBuilder } from "@angular/forms";
import { MatDialog, MatDialogModule, MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { NewsService } from "../services/news.service";
import { AddNewsComponent } from "./add-news.component";

const data = { user_id: 1 };
const dialogMock = {};
const newsServiceStub: Partial<NewsService> = {
  selectedFile: <File>{}
};
const filledForm = {
  user_id: 1,
  title: 'Title',
  content: 'Content',
  tags: 'Tags'
}

describe('AddNewsComponent', () => {
  let component: AddNewsComponent;
  let fixture: ComponentFixture<AddNewsComponent>

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [MatDialogModule],
      declarations: [AddNewsComponent],
      providers: [
        { provide: MatDialogRef, useValue: dialogMock},
        { provide: MAT_DIALOG_DATA,  useValue: data },
        { provide: NewsService, useValue: newsServiceStub },
        FormBuilder
      ]
    }).compileComponents();
    fixture = TestBed.createComponent(AddNewsComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should create a form with 3 controls', () => {
		expect(component.addNewsForm.contains('title')).toBeTruthy();
		expect(component.addNewsForm.contains('content')).toBeTruthy();
    expect(component.addNewsForm.contains('tags')).toBeTruthy();
	});

	it('title should be required', () => {
		const control = component.addNewsForm.get('title');
		control!.setValue('');
		expect(control!.valid).toBeFalsy();
	});
})
