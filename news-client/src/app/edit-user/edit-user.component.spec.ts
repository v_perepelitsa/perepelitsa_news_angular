import { TestBed } from "@angular/core/testing";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { UserService } from "../services/user.service";
import { EditUserComponent } from "./edit-user.component";

const userServiceStub: Partial<UserService> = {
  selectedFile: <File>{}
};

const data = {
  login: 'Bulgakov'
}

const file = new File([new Blob(['test'])], 'test');

describe('EditUserComponent', () => {
  let component: EditUserComponent;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EditUserComponent],
      providers: [
        { provide: MatDialogRef, useFactory: () => jasmine.createSpyObj('MatDialogRef', ['close']) },
        { provide: MAT_DIALOG_DATA,  useValue: data },
        { provide: UserService, useValue: userServiceStub }
      ]
    }).compileComponents();
    const fixture = TestBed.createComponent(EditUserComponent);
    component = fixture.debugElement.componentInstance;
  });

  it('should select file', () => {
    const newFile = {target: { files: [file]} };
    component.onFileSelected(newFile);
    expect(component.selectedFile).toEqual(file);
  });
});
